import mysql.connector
from operator import itemgetter

mydb = mysql.connector.connect(
    host = "localhost",
    user = "root",
    passwd = "password",
    database = "test",

)

mycursor = mydb.cursor()

#     Keywords                   Importance Factor(IF)
# name related keywords                 +1.0 0.48
# location related keywords             +0.5 0.32
# other keywords                        +0.2 0.10

print("====Enter the keywords which describe the target user in best possible way====")
print("=================Enter 'NONE' if no value present for a Field=================\n")

inp_str = input("Name Related Keywords: ")
names = inp_str.split(" ")
inp_str = input("Location Related Keywords: ")
locations = inp_str.split(" ")
inp_str = input("Other Keywords: ")
others = inp_str.split(" ")
print("\n==========================Result==========================\n")

score_list = []

for keyword in names:
    mycursor.execute("SELECT * FROM test.gt WHERE CONCAT_WS('', g__displayname,g__firstname,g__lastname,t__fullname,t__screen_name) LIKE %s;", ("%" + keyword + "%", ))
    res = mycursor.fetchall()
    
    for r in res:
        rl = list(r)
        # print(rl)
        score = 1
        for field in rl:
            for loc in locations:
                if loc.upper().lower() in field.upper().lower():
                    score += 0.5
                    
        for field in rl:
            for oth in others:
                if oth.upper().lower() in field.upper().lower():
                    score += 0.2

        rl.insert(0,score)
        score_list.append(rl)
        # print(rl)

sorted_list = sorted(score_list, key = itemgetter(0), reverse=True)
for x in sorted_list:
    print(x)
    print("==========================================================")    
    


# print(mydb)

# mycursor.execute("CREATE DATABASE test")
# Now we import our dataset/table in this database
# We can use gui tools like phpmyadmin or mysql workbench for
# importing our data into it.
# Below is an example of creating a new table in our test database

# mycursor.execute("SHOW DATABASES")

# for db in mycursor:
#     print(db)

# mycursor.execute("CREATE TABLE students (name VARCHAR(255), age INTEGER(10))")

# formula = "INSERT INTO students (name, age) VALUES (%s, %s)"

# students = [
#     ("a", 11),
#     ("b", 12),
#     ("c", 14),
#     ("d", 14),
#     ("e", 31),
#     ("f", 11),
#     ("g", 12),
    
# ]

# mycursor.executemany(formula, students)

# mydb.commit()

# mycursor.execute("SELECT * FROM students")

# res = mycursor.fetchall()
# for r in res:
#     print(r)

# mycursor.execute("SELECT column_name FROM information_schema.columns WHERE table_schema = 'test' AND table_name = 'gt' ;")
# mycursor.execute("SHOW COLUMNS FROM gt")
# fields = mycursor.fetchall()

